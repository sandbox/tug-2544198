# Nirror #

This module allows you to integrate the Nirror script tag into your Drupal site.

Nirror is a customer support service for your website.
Chat with your visitors and watch their screen live.

Do not waste any more time trying to understand what process your customers went
through before they asked for help. You just watch what happened in past visits
and use this to forge a very precise answer your customer will love.

## Installation ##

This section describes how to install the module and get it working.

1. Install the module on your Drupal install.
You can follow [Drupal's documentation](https://www.drupal.org/documentation/install/modules-themes/modules-7#upload-the-module) on the subject.
2. Click on the menu item "Configuration".
3. Paste your "Site ID" in the first text field and click "Save Changes"
to get started.

## Support ##
Contact us directly via email for support
[support@nirror.com](mailto:support@nirror.com).
