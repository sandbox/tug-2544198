<?php

/**
 * @file
 * This file contains all the admin-related callbacks
 */


/**
 * Main settings form page.
 */
function nirror_admin_settings_form($form) {
  $profile = variable_get('nirror_profile', array());

  // Fill in profile defaults to ensure all keys exist.
  $profile += array(
    'siteid' => '',
    'custom' => '',
    'path_visibility' => '',
    'paths' => '',
  );

  $form['siteid'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#description' => t('This is the unique ID of your site. It can be found in Nirror App on the My Sites page'),
    '#required' => TRUE,
    '#default_value' => $profile['siteid'],
  );

  $form['custom'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom configuration'),
    '#description' => t('Enter custom code which uses Nirror client API'),
    '#required' => FALSE,
    '#default_value' => $profile['custom'],
    '#wysiwyg' => FALSE,
  );

  $form['path_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Run Nirror on specific pages'),
    '#options' => array(
      0 => t('Run Nirror on every page except the listed pages.'),
      1 => t('Run Nirror only the listed pages.'),
    ),
    '#default_value' => $profile['path_visibility'],
  );

  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#required' => FALSE,
    '#default_value' => $profile['paths'],
    '#wysiwyg' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}


/**
 * Validate handler for settings form.
 */
function nirror_admin_settings_form_validate($form, &$form_state) {
  if (!preg_match('/^[a-f0-9]{24}$/', $form_state['values']['siteid'])) {
    form_set_error('name', t('The Site ID only contains lower case letters, numbers and must be 24 characters long.'));
  }
}


/**
 * Submit handler for settings form.
 */
function nirror_admin_settings_form_submit($form, $form_state) {

  $profile = array(
    'siteid'  => $form_state['values']['siteid'],
    'custom'  => trim($form_state['values']['custom']),
    'paths' => trim($form_state['values']['paths']),
    'path_visibility' => $form_state['values']['path_visibility'],
  );

  variable_set('nirror_profile', $profile);
}
